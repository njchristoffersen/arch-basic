# /bin/sh
## connect to the internet
# iwctl
# [iwd]# station wlan0 connect NETGEAR37

## install git
# pacman -Sy git

## download this repository
# git clone https://gitlab.com/njchristoffersen/arch-basic

## run the script
# chmod +x ~/base.sh
# ~/base.sh

## actually, just first get into the pacstrapped stuff
# fdisk /dev/nvme0n1

## create minimum 2 partitions
## /dev/nvme0n1p1 512MiB boot partition
# mkfs.fat -F 32 /dev/nvme0n1p1
## /dev/nvme0n1pX encrypted filesystem
# cryptsetup luksFormat /dev/nvme0n1pX
# cryptsetup luksOpen /dev/nvme0n1pX cryptroot

##      if using btrfs
#      mkfs.btrfs /dev/mapper/cryptroot
#      MORE GOES HERE
       
##      if using ext4
#      mkfs.ext4 /dev/mapper/cryptroot
#      mkswap /dev/nvme0n1pY
#      swapon /dev/nvme0n1pY
#      mount /dev/mapper/cryptroot /mnt
#      mkdir /mnt/boot
#      mount /dev/nvme0n1p1 /mnt/boot

## download the bare necessities
# pacstrap -K /mnt base linux linux-firmware git vim
# genfstab -U /mnt >> /mnt/etc/fstab
# arch-chroot /mnt

ln -sf /usr/share/zoneinfo/America/Denver /etc/localtime
hwclock --systohc
echo '\nen_US.UTF-8 UTF-8' >> /etc/locale.gen
locale-gen
echo "LANG=en_US.UTF-8" >> /etc/locale.conf
echo "arch" >> /etc/hostname
echo "127.0.0.1 localhost" >> /etc/hosts
echo "::1       localhost" >> /etc/hosts
echo "127.0.1.1 arch.localdomain arch" >> /etc/hosts
echo root:password | chpasswd

pacman -S grub efibootmgr networkmanager network-manager-applet dialog wpa_supplicant mtools dosfstools base-devel linux-headers avahi xdg-user-dirs xdg-utils gvfs gvfs-smb nfs-utils inetutils dnsutils bluez bluez-utils cups hplip alsa-utils pipewire pipewire-alsa pipewire-pulse pipewire-jack bash-completion openssh rsync reflector acpi acpi_call tlp virt-manager qemu qemu-arch-extra edk2-ovmf bridge-utils dnsmasq vde2 openbsd-netcat iptables-nft ipset firewalld flatpak sof-firmware nss-mdns acpid os-prober ntfs-3g terminus-font ranger xorg i3-wm

grub-install --target=x86_64-efi --efi-directory=/boot --bootloader-id=GRUB #change the directory to /boot/efi is you mounted the EFI partition at /boot/efi

grub-mkconfig -o /boot/grub/grub.cfg

systemctl enable NetworkManager
systemctl enable bluetooth
systemctl enable cups.service
systemctl enable sshd
systemctl enable avahi-daemon
systemctl enable tlp # You can comment this command out if you didn't install tlp, see above
systemctl enable reflector.timer
systemctl enable fstrim.timer
systemctl enable libvirtd
systemctl enable firewalld
systemctl enable acpid

useradd -m nco
echo nco:password | chpasswd

echo "nco ALL=(ALL) ALL" >> /etc/sudoers.d/ermanno


printf "\e[1;32mDone! Type exit, umount -a and reboot.\e[0m"

